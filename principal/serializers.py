from rest_framework import serializers
from .models import Nota, Categoria

# class NotaSerializer(serializers.ModelSerializer):
# 	class Meta:
# 		model = Nota
# 		fields = ('titulo','description','categoria')
# 		# exclude = ('',)
# 		depth = 1

class NotaSerializer(serializers.ModelSerializer):
	# source es un metodo del modelo
	titulo_min = serializers.ReadOnlyField(source='titulo_minusculas',)
	# campo calculado aqui mismo
	saludo = serializers.SerializerMethodField('saludar',)

	class Meta:
		model = Nota
		fields = ('titulo','description','categoria', 'titulo_min', 'saludo')

	def saludar(self, obj):
		return 'hola mundo'

class CategoriaSerializer(serializers.HyperlinkedModelSerializer):
	# notas = serializers.StringRelatedField(many=True, read_only=True)
	notas = NotaSerializer(many=True)

	class Meta:
		model = Categoria


# un ejemplo de serializer normal
class EjemploSerializer(serializers.Serializer):
	nombre = serializers.CharField(max_length=20)
	email = serializers.EmailField()