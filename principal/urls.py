from django.conf.urls import include, url
from django.contrib import admin
from rest_framework import routers

from .views import NotaViewSet, CategoriaViewSet
from .models import Nota

router = routers.DefaultRouter()
router.register(r'notas', NotaViewSet, base_name=Nota)
router.register(r'categorias', CategoriaViewSet)

urlpatterns = [
	url(r'^api/', include(router.urls)),
]
