from rest_framework import viewsets
from rest_framework.response import Response
from django.shortcuts import get_object_or_404

from .models import Nota, Categoria
from .serializers import NotaSerializer, CategoriaSerializer

class CategoriaViewSet(viewsets.ModelViewSet):
	queryset = Categoria.objects.all()
	serializer_class = CategoriaSerializer

# class NotaViewSet(viewsets.ModelViewSet):
# 	queryset = Nota.objects.all()
# 	serializer_class = NotaSerializer

# escribiendo un viewset a pelo, revisa las urls tiene un base_name
class NotaViewSet(viewsets.ViewSet):
	# listar
	def list(self, request):
		#if request.user.is_authenticated():
		notas = Nota.objects.all()
		serializer = NotaSerializer(notas, many=True)
		
		return Response(serializer.data)
	# ver detalle
	def retrieve(self, request, pk=None):
		nota = get_object_or_404(Nota, pk=pk)
		serializer = NotaSerializer(nota)

		return Response(serializer.data)

	# create, update, destroy, partial_update...
